
export IIHE_TOOLS_BASE_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export PATH=$IIHE_TOOLS_BASE_DIR/bin:$PATH
export PYTHONPATH=$IIHE_TOOLS_BASE_DIR/common/python:$IIHE_TOOLS_BASE_DIR/web_interface/app:$IIHE_TOOLS_BASE_DIR/plotting_scripts:$IIHE_TOOLS_BASE_DIR/psu_control:$PYTHONPATH

