import json, time, os
from SensorTag import SensorTag
from dummy_db import dummy_db


config = json.load(open(os.environ["IIHE_TOOLS_BASE_DIR"]+"/common/config/sensortag_conf.json","r"))
sensors = []
for add in config["Addresses"]:
    name = config["Addresses"][add]
    try:
        sens = SensorTag(add,name)
        if sens.connect() == True:
            sensors.append(sens)
    except:
        print("Fatal, unable to connect!")
print(sensors)
struct = [('date', 4)]
for sens in sensors:
    struct.append((f"T_{sens.name}",2))
    struct.append((f"H_{sens.name}",2))

db = dummy_db(file_name = os.environ["IIHE_TOOLS_BASE_DIR"]+"/"+config["db"], structure = struct)

try:
    while True:
        sens.turn_on()
        entry = {}
        entry["date"] = int(time.time())
        for sens in sensors:
            entry[f"T_{sens.name}"], entry[f"H_{sens.name}"] = sens.get_values()
        sens.turn_off()
        print(entry[f"T_{sens.name}"]/65536.0*165-40)
        db.add(entry)
        db.write_to_file()
        print("New entry...")
        time.sleep(config["sleep"])

except Exception as e:
    for sens in sensors:
        print(f"Disconnecting {sens}")
        sens.disconnect()
