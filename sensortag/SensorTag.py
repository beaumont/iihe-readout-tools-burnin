import struct, sys, traceback
from bluepy.btle import UUID, Peripheral, BTLEException
 
def TI_UUID(val):
    return UUID("%08X-0451-4000-b000-000000000000" % (0xF0000000+val))

config_uuid = TI_UUID(0xAA22)
data_uuid = TI_UUID(0xAA21)

sensorOn  = struct.pack("B", 0x01)
sensorOff = struct.pack("B", 0x00)



class SensorTag:
    def __init__(self,bt_add, name):
        print(f"ST {name} Trying to connect to {bt_add}")
        self.name = name
        self.bt_add = bt_add
        self.connected = False
        self.dev = None
    
    def connect(self):
        try:
            self.dev = Peripheral(self.bt_add)
            self.connected = True
            return True
        except:
            self.dev = None
            self.connected = False
            return False

    def turn_on(self):
        if not self.connected:
            return False
        try:
            ch = self.dev.getCharacteristics(uuid=config_uuid)[0]
            ch.write(sensorOn, withResponse=True)
            return True
        except:
            return False

    def get_values(self):
        try:
            ch = self.dev.getCharacteristics(uuid=data_uuid)[0]
            rawVals=ch.read()
            temp = ((rawVals[1]<<8)+rawVals[0])#/65536.0*165-40
            hum  = ((rawVals[3]<<8)+rawVals[2])#/ 65536.0 * 100
            return temp,hum
        except:
            return 0,0xFFFF    
    def turn_off(self):
        try:
            ch = self.dev.getCharacteristics(uuid=config_uuid)[0]
            ch.write(sensorOff, withResponse=True)
            return 1
        except:
            return 0
     
    def disconnect(self):
        try:
            self.dev.disconnect()
            return 1
        except:
            return 0
