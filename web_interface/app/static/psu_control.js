function set_channel_status(channel,status){
  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
      get_live_data();
    }
  }
  if (status==0){
    xmlhttp.open("GET", "send_psu_cmd?cmd=turn_off+"+channel , false );
  }
  else{
    xmlhttp.open("GET", "send_psu_cmd?cmd=turn_on+"+channel , false );
  }
  xmlhttp.send(null);
}

function restart_service(){
  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
    }
  }
  xmlhttp.open("GET", "send_psu_cmd?cmd=restart" , false );
  xmlhttp.send(null);
}

function change_voltage(channel,label_id){
  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
      get_live_data();
    }
  }
  xmlhttp.open("GET", "send_psu_cmd?cmd=set_v+"+channel+"+"+document.getElementById(label_id).value , false );
  xmlhttp.send(null);
}


function get_live_data(){
    //alert("Salut?");
    url="get_live_psu"
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
            var data = JSON.parse(xmlhttp.responseText);
            if (data["1_status"] == "1"){
              document.getElementById("1_status").innerHTML="Enabled";
              document.getElementById("1_status").style.color="green";
              document.getElementById("1_change_status").innerHTML="Disable";
              document.getElementById("1_change_status").onclick = function(){set_channel_status(1,0);}; 

            }
            else{
              document.getElementById("1_status").innerHTML="Disabled";
              document.getElementById("1_status").style.color="grey";
              document.getElementById("1_change_status").innerHTML="Enable";
              document.getElementById("1_change_status").onclick = function(){set_channel_status(1,1);}; 
            }
            document.getElementById("1_set_v").innerHTML = data["1_set_v"];
            document.getElementById("1_mon_v").innerHTML = data["1_mon_v"];              
            document.getElementById("1_mon_i").innerHTML = data["1_mon_i"];


            if (data["2_status"] == "1"){
              document.getElementById("2_status").innerHTML="Enabled";
              document.getElementById("2_status").style.color="green";
              document.getElementById("2_change_status").innerHTML="Disable";
              document.getElementById("2_change_status").onclick = function(){set_channel_status(2,0);}; 

            }
            else{
              document.getElementById("2_status").innerHTML="Disabled";
              document.getElementById("2_status").style.color="grey";
              document.getElementById("2_change_status").innerHTML="Enable";
              document.getElementById("2_change_status").onclick = function(){set_channel_status(2,1);}; 
            }
            document.getElementById("2_set_v").innerHTML = data["2_set_v"];
            document.getElementById("2_mon_v").innerHTML = data["2_mon_v"];              
            document.getElementById("2_mon_i").innerHTML = data["2_mon_i"];



            if (data["3_status"] == "1"){
              document.getElementById("3_status").innerHTML="Enabled";
              document.getElementById("3_status").style.color="green";
              document.getElementById("3_change_status").innerHTML="Disable";
              document.getElementById("3_change_status").onclick = function(){set_channel_status(3,0);}; 

            }
            else{
              document.getElementById("3_status").innerHTML="Disabled";
              document.getElementById("3_status").style.color="grey";
              document.getElementById("3_change_status").innerHTML="Enable";
              document.getElementById("3_change_status").onclick = function(){set_channel_status(3,1);}; 
            }
            document.getElementById("3_set_v").innerHTML = data["3_set_v"];
            document.getElementById("3_mon_v").innerHTML = data["3_mon_v"];              
            document.getElementById("3_mon_i").innerHTML = data["3_mon_i"];


            if (data["hv_status"] == "1"){
              document.getElementById("hv_status").innerHTML="Enabled";
              document.getElementById("hv_status").style.color="red";
              document.getElementById("hv_change_status").innerHTML="Disable";
              document.getElementById("hv_change_status").onclick = function(){set_channel_status("hv",0);}; 

            }
            else{
              document.getElementById("hv_status").innerHTML="Disabled";
              document.getElementById("hv_status").style.color="grey";
              document.getElementById("hv_change_status").innerHTML="Enable";
              document.getElementById("hv_change_status").onclick = function(){set_channel_status("hv",1);}; 
            }
            document.getElementById("hv_set_v").innerHTML = data["hv_set_v"];
            document.getElementById("hv_mon_v").innerHTML = data["hv_mon_v"];              
            document.getElementById("hv_mon_i").innerHTML = data["hv_mon_i"];
            
            document.getElementById("hv_ramp").innerHTML = data["hv_ramp"];
            document.getElementById("hv_error").innerHTML = data["hv_error"];
            if (data["hv_ramp"] == "" && data["hv_error"] == ""){
              document.getElementById("hv_status_line").style.display = "none";
            }
            else{
              document.getElementById("hv_status_line").style.display = "block";
            }
        }
    }
    xmlhttp.open("GET", url , false );
    xmlhttp.send(null);
}


document.getElementById("1_change_voltage").onclick = function(){change_voltage(1,"1_voltage_select");}; 
document.getElementById("2_change_voltage").onclick = function(){change_voltage(2,"2_voltage_select");}; 
document.getElementById("3_change_voltage").onclick = function(){change_voltage(3,"3_voltage_select");}; 
document.getElementById("hv_change_voltage").onclick = function(){change_voltage("hv","hv_voltage_select");}; 

document.getElementById("refresh").onclick = function(){get_live_data()};
document.getElementById("restart").onclick = function(){restart_service()};


function autorun(){
  if (document.getElementById("auto_refresh").checked == true){
    get_live_data();
  }
}

get_live_data();
window.setInterval(autorun,5000);

