from os import path
from os import SEEK_END
import json
from datetime import datetime

def convert(meas_name,value,type = "float",keep_units = True):
    if meas_name == "timestamp":
        if type == "str":
            return datetime.fromtimestamp(value).strftime("%Y:%m:%d :%H:%M:%S")
        else:
            return value
    elif "_T" in meas_name or "PT_" in meas_name:
        val = round(value*0.1-50,1)
        if type == "str":
            return f"{val:.1f}"+ keep_units*" °"
        else:
            return val
    elif "_H" in meas_name:
        val = round(value*0.1,1)
        if type == "str":
            return f"{val:.1f}"+keep_units*" %"
        else:
            return val
    elif "acc" in meas_name:
        val = round(value/30.,1)
        if type == "str":
            return f"{val:.1f}"
        else:
            return val
    else:
        if type == "str":
            return f"{value}"
        else:
            return value



class event:
    def __init__(self, struct, raw_data):
        self.struct = struct
        self.load(raw_data)
        
    def load(self,raw_data):
        self.data = {}
        if 8*len(raw_data) < self.struct["size"]:
            print(f"Error ! Input data too short ! {len(raw_data)} < {self.struct['size']}")
            return

        bit_string = ""
        for bb in range(int(self.struct["size"]/8)):
            xxx = bin(raw_data[bb])[2:]
            bit_string+=(8-len(xxx))*"0"+xxx
        self.data = {}

        bit_index = 0
        for entry_name,entry_size in self.struct["split"]:
            xxx = int(bit_string[bit_index:bit_index+entry_size],2)
            self.data[entry_name] = convert(entry_name,xxx)
            bit_index+=entry_size
            
    def keys(self):
        return self.data.keys()

    def __getattr__(self, attr):
        return(self.data[attr])

    def __getitem__(self,key):
        return(self.data[key])

    def __repr__(self):
        ss = ""
        for key in self.data:
            ss+=f"{key} = {self.data[key]}  "
        return(ss)


class magnet_db:
    def __init__(self,config_file,file_name):
        self.file_name = file_name
        self.f = None

        self.conf = json.load(open(config_file,"r"))

        self.event_length = int(self.conf["evt_def"][0]["size"]/8)


        self.temp_data = None
        self.index = None
        self.data = []
        self.structure = self.conf["evt_def"][0]

        self.n_events  = 0
        self.start_pos = 0
        self.stop_pos  = 0


        if path.exists(self.file_name):
            self.refresh_from_file()

    def refresh_from_file(self):
        if self.f:
            self.f.close()

        self.f=open(self.file_name,"rb")
        self.start_pos = self.f.tell()
        self.stop_pos = self.f.seek(0,SEEK_END)
        self.f.seek(self.start_pos)
        
        self.number_of_events = (self.stop_pos - self.start_pos)/self.event_length
        if not self.number_of_events == int(self.number_of_events):
            print ("Error, number of event is not an int. Corrupted file or bad header?")
        self.number_of_events = int(self.number_of_events)

    def get_index(self):
        self.index = {}
        if self.structure == None:
            return
        pos = 0
        for name,size in self.structure:
            self.index[name]=(size,pos)
            pos+=size

    def get_entry(self,position):
        self.move_to(position)
        return self.read_event()

    def read_event(self):
        if self.f == None:
            self.refresh_from_file()
            print("Error, file is not opened...")
            print("To be implemented")

        raw_data = self.f.read(self.event_length)
        if len(raw_data) == 0:
            return None
        return(event(self.structure,raw_data))

    def move_to(self,event_position):
        if event_position >= 0:
            pointer_pos = self.start_pos + event_position*self.event_length
        else:
            pointer_pos = self.stop_pos + event_position*self.event_length

        if pointer_pos > self.stop_pos or pointer_pos < self.start_pos:
            print("Error, unable to move to event #{}".format(event_position))
        else:
            self.f.seek(pointer_pos)

    def skip(self,number_of_events):
        number_of_events = int(number_of_events)
        if not self.f:
            print("Opening file...")
            self.f.refresh_from_file()
        new_pos = self.f.tell() + number_of_events*self.event_length
        if new_pos < self.start_pos:
            print("Setting back to first entry")
            new_pos = self.start_pos
        elif new_pos > self.stop_pos:
            print("Warning, reached the end of the file")
        self.f.seek(new_pos)




if __name__ == "__main__":
    #dd = dummy_db(file_name = "new.db", structure = [('date', 4), ('temp', 2), ('hum', 2)])
    dd = magnet_db(file_name="/home/mdelcourt/Documents/VUB/Magnet_test/Monitoring/interface/default.bin", config_file="/home/mdelcourt/Documents/VUB/Magnet_test/Monitoring/interface/conf.json")
    for i in range(1000):
        #event = {"date":i,"temp":i,"hum":i}   
        print(dd.read_event())
        #dd.skip(9)

    #dd.write_to_file()
    print(dd.number_of_events)
    #dd.move_to(-1)
    #print (dd.read_event())
