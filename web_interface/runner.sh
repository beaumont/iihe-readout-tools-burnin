export HOSTNAME=`hostname` 
export SERVER_IP="localhost";
export WEB_DIR=$IIHE_TOOLS_BASE_DIR/web_interface;
cd $WEB_DIR;

export FLASK_APP=main.py
export FLASK_ENV=development
export FLASK_DEBUG=1
flask run -h $SERVER_IP -p 55555
