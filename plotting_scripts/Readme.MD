# My 2S noise plotting scripts
## Introduction
This git repository compiles my helper scripts used to produce custom noise plots for IIHE 2S modules.

The workflow works as follows : 
 * Ph2_ACF is used to perform noise measurements
 * `process_last_run.py` moves the results to a user-friendly location and prints an overview of the results
 * `plot_2S_noise.py` merges 1 or more measurements to provide a noise profile + RMS
 * `plot_2S_trend.py` merges differents measurements at different biases to provide a noise trend.

This document will briefly give an overview of the these three scripts and how to use them.

These scripts are still work in progress, so they might very well be changed (and unfortunately, the documentation might not be 100% up to date). Feel free to contact me if in need of additional information.


## Installation
To install these scripts (ie, have them usable anywhere from your sessions), download them from github, render them executable (`chmod +x process_last_run.py plot_2S_noise.py plot_2S_trend.py`) and add a line in your `.bashrc` file to add this location to your `PATH` variable. For example :
``` bash
export PATH="/home/your_username/path/blahblah/please_change_this_path/ph2_plotting_scripts":$PATH
```
Don't forget to also run this if you want to use the scripts in the same session as the installation process (or to restart your session).

## Run "processing"
While performing a 2S noise measurement, some critical steps are required in my workflow and not directly available in Ph2_ACF.

I want:
* The calibration output to be in a human-readable and uniquely identifiable location, easily correlated with elog entries
* The calibration output to be tagged with biasing information from a non-remotely operated PSU
* A summary of the measurement to immediatly see any disconnected strips/coupled strips, and to be able to easily assess the validity of the measurement

To perform these simple tasks, the `process_last_run.py` script was performed. 

In order to use it, you have to be situated in the parent directory of `Ph2_ACF`, where a `Results` directory was created. Then, from there, run : 
```process_last_run.py USER_FRIENDLY_INFO```

This script will look into `Ph2_ACF/Results`, where the calibration output is supposed to be, look for the newest folder, copy it to `Results/DATE_STRING/USER_FRIENDLY_INFO`, where `DATE_STRING` is the current day in the `YYYYMMDD` format, and `USER_FRIENDLY_INFO` is given by the user to the script. If such a folder already exists, the `USER_FRIENDLY_INFO` value will be appended with an underscore and an increasing number, starting with `2`.

The script will also parse the `Hybrids.root` file produced, and print to the user, for each hybrid/sensor, the average, RMS, min and max of noise and offsets. This info is useful to make sure you don't have a single strip with abnormal values (which is expected if there is a chip/sensor/bonding defect).

While the `USER_FRIENDLY_INFO` can be whatever the user desires, it is recommended to have the following structure : `USER_FRIENDLY_INFO = PREFIX_BIASV`, for example : `FM3_200V`.

The downstream scripts use this user information to get the bias voltage (any part of the name with `#number#V`, where parts of the name are found by splitting on underscores), and a filter selecting the prefix can be applied.

## Plotting
### Introduction
If all your noise measurements went well (or not), you will very probably want to have a look at the distributions. While you can very easily look into `Hybrids.root` for the relevant distributions, merging different runs for more in-depth behaviours requires some level of scripting. Here are mine.

I usually produce two different types of plots : 
* Noise profile plots, giving the noise value as a function of strip #
* Noise trend plots, giving the average noise as a function of bias voltage

These are done using `plot_2S_noise.py` and `plot_2S_trend.py` respectively.

### Common 

The overall usage of both scripts is very similar. Once you have downloaded all your measurements into one neat location (for example, by copying the dated folder results on your computer), you can run these scripts from anywhere (assuming you perfomed the "installation" step).

The scripts can be ran as any other command, requiring only one argument, the input directory (where your results are), using the `-i` option, using either a local or global path. For example, for the noise profile script : 

```bash
plot_2S_noise.py -i 19700101/
```

Many other options are available to customize the output plots. They can be listed using the `-h` option.

The options common to the two scripts are :
   * `--output PATH` or `-o PATH`, specifies the output directory where the images will be stored
   * `-b`, if used, no plots will be flashed to screen while running.
   * `--legend legend_pos` with `legend_pos` being either `top` or `bottom`, specifies where the legend will be drawn.
   * `--y_min val`, `--y_max val`, specifies the y-axis user range
   * `--x_min val`, `--x_max val`, specifies the x-axis user range
   * `--hybrids hyb`, with hyb being `0`,`1` or `both`, specifies which hybrids to draw
   * `--sensor sens`, with sens being `even`,`odd` or `both`, specifies which sensor to draw
   * `--out_format form`, with form beign `pdf`, `png` or `both`, specifies what output format is to be used.
   * `--size_x val`, `--size_y val`, selects the size (in px) of the output canvas. This also sets the plot ratio.
   * `--prefix prefix`, only use data from folders containing the given prefix.
   * `--title title`, sets the title of the plot to the string given

### Noise Profile

The noise profile script gathers data from the given input folder, splits the runs depending on the bias voltage, then merges these runs to provide an uncertainty estimate on the noise values returned.

The central value of each strip is given by the average of the measurements for this strips, and the uncertainty displayed is the uncertainty of this average, computed as the strip RMS divided by the square root of the number of measurements. If only one run is given for a particular bias point, no uncertainty is displayed.

Additionaly to the common options described earlier, the following options can be used :
   * `--merge`, shows both hybrids on the same plot (can become messy...)
   * `--hide_cbc`, removes the CBC boundary lines on the plot
   * `--print_prefix`, includes the prefix in the title
   * `--hide_voltage`, removes the bias voltage information from the title

Here is an example of the script being ran :

``` bash
plot_2S_noise.py -i 20210920/ -o 0V/ -b --legend bottom --size_x 1000 --size_y 648 --y_min 20 --y_max 35
```

### Noise trend

The noise profile script gathers data from the given input folder, splits the runs depending on the bias voltage, then plots the average noise as a function of bias voltage.

The central value of each bias point is given by the average of the noise for this measurement, and the uncertainty displayed is the total strip RMS. By default, multiple measurements can coexist on a single bias point. These can be merged through the `--merge` option.

Additionaly to the common options described earlier, the following options can be used :
 * `--merge`, merges multiple measurements done at a single bias point
 * `--line`, adds lines going through the measurements
 * `--depletion`, performs a data transformation to look for the full depletion voltage. The noise is decreased by a constant value of 2 (to approximately remove CBC noise contribution), and is then inverted. This value is then shown as a function of the square root of the bias voltage. The expected behaviour, if all other noise sources are neglected (obviously an approximation), is to have a linear increase up to the full depletion voltage where the curve will become constant.

